
### Purpose ###

Convert audio formats supported  by `ffmpeg` to the
[compact](like://www.opus-codec.org/comparison/) audio format `opus`.  Most
likely saving you disk space. Which of course is finite.

Quoting the `--help` text:

		More files, less space. Not all audio
		should be of the best possible quality.
		Generally most won't hear any difference
		unless the quality is very poor.

This utility is a "glorified shell script" that tries to make it easier to
convert files with  `libopus`. I.e. "_don't worry about typing, or remembering
lengthy command-line options, just convert._"

I should however say that it's far from perfect. I've used it a lot though, and
I fix quirks as I go along. (Which might take place over an unspecified period
of time. But at this point I've tweaked it about ~1 year.)

_I figure that making it public will prompt me to improve stuff. Someone might
actually see it..._

### Quick Usage Examples ###

***Use*** `mkopus --help` ***for the "real deal"***.

**Setup**:  
_I'd just do this manually for precision, but "do what thou wilt"._  
Installs at `$HOME/bin` or `/usr/local/bin`.

`chmod +x setup.sh`  
`./setup.sh`

Since the script assumes that you work in your working directory (like a cloned
`git` repository) you probably should do this too:  

`chmod +x mkopus.sh`

**Use `mkopus`**:  
If you didn't install `mkopus` to `$PATH` use `./mkopus.sh`.

Locally in a given directory with `flac`-files (where bitrate: `128k`,
compression: `9`):  
`mkopus flac 128k 9`

As above but use `-d` to delete the original files:  
`mkopus flac 128k 9 -d`

...and recursively (i.e. you're in directory 'This_Band' with Albums '2000 - A,
2004 - B, 3400 - C'):  
`mkopus flac 128k 9 -d -r`

...or as above using sensible (sort of) bitrate/compression defaults with `-a`:  
`mkopus flac -a -d -r`

...or as a above clean a part of filename, like the band name 'This_Band' or an
arbitrary string, with `-b` and `-c` (`c`lean). Both because `-b` might be
useful for other stuff which doesn't imply `-c`.  
`mkopus flac -a -b "-This_Band" -c -r -d`


**General syntax**:

`mkopus <format> <bitrate[k]> <compression> <options>`

Both `./setup.sh` and `mkopus` takes an `-h` argument.


### Quirks / Bugs / Comments ###

Note that options like `-d` come _after_ the `format` (ex. `flac`). Partly
because it intuitive, partly because I've not come around to simplifying the
passing of all arguments yet.

For what it's worth I think like this: "_State this `<format>`, etc, then
manipulate that (ex. delete `-d` the files)._"

Will strip `0`:s because `00001` just looks stupid while `01` does not. This
only strip three zeros. Use `--no-num` for files starting with non-digits. Will
make this more flexible later on.

##### Comments #####

`mkopus` was written because the `ffmpeg` syntax can be confusing. Try to
process multiple files and add something like `youtube-dl`'s output the mix even
more so. It gets overly detailed pretty quickly.

That's what I try to avoid here. Do less, be simple, use more or less obvious
syntax and options. I.e. just "make opus".

Don't bother learning 1000+ words of some output template system when some regex
will do. Use plainly worded descriptions for a few options. Avoid awkward
aliases or long one-liners. Work in a way that makes it easier to sort files in
directories like songs and albums.

Slight **warning** about `-d`+`-r` (recursive deletion): it _could_ result in
data loss. I suppose temporary backing files up could prevent this (Might make
it an option).

Personally I've done this in directories with hundreds of files without anything
nasty happening. _However_, deeply nested directory trees might cause problems
at the moment.

### Requirements ###

Not a lot of them. A `bash` compatible shell and `ffmpeg`. (And if it breaks:
patience, or use `--fist`).


### License ###

That [same as `ffmpeg`](https://ffmpeg.org/legal.html) which in practice means
[GPLv2+](https://www.gnu.org/licenses/old-licenses/gpl-2.0.html) on my system.

See [COPYING](https://gitlab.com/kxxvii/mkopus.sh/-/blob/master/COPYING) for
more information.

