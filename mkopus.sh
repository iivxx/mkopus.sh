#!/bin/bash


# -:::::::::::::::::::::::::::::::::::-
# ¦                                   ¦
# ¦           P U R P O S E           ¦
# ¦                                   ¦
# ¦  Convert audio formats supported  ¦
# ¦  by 'ffmpeg' to the compact audio ¦
# ¦  format 'opus'.                   ¦
# ¦  Useful for saving disk space.    ¦
# ¦                                   ¦
# ¦  This utility is a glorified      ¦
# ¦  shell script that tries to make  ¦
# ¦  it easier to convert files with  ¦
# ¦  'libopus'. I.e. "don't worry     ¦
# ¦  about typing, just convert."     ¦
# ¦                                   ¦
# ¦  TODO [06-22-22]:                 ¦
# ¦    * Fix band rename to prevent   ¦
# ¦      overwriting of current files ¦
# ¦    * Account for existing opus    ¦
# ¦      where appropriate            ¦
# ¦    * Improve pattern processing   ¦
# ¦    * Make options more flexible,  ¦
# ¦      i.e. the positional args     ¦
# ¦                                   ¦
# ¦  /iivxx, 2022                     ¦
# ¦                                   ¦
# -:::::::::::::::::::::::::::::::::::-

# -:::::::::::::::::::::::::::::::::::-
# ¦                                   ¦
# ¦          L I C E N S E            ¦
# ¦                                   ¦
# ¦  The same license as 'ffmpeg',    ¦
# ¦  which is the GPLv2+:             ¦
# ¦  See: ffmpeg.org/legal.html       ¦
# ¦                                   ¦
# ¦  "GNU General Public License as   ¦
# ¦  published by the Free Software   ¦
# ¦  Foundation; either version 2 of  ¦
# ¦  the License, or (at your option) ¦
# ¦  any later version."              ¦
# ¦                                   ¦
# ¦  Copyright 2022, iivxx            ¦
# ¦  URL: gitlab.com/iivxx            ¦
# ¦                                   ¦
# ¦  See also: COPYING                ¦
# ¦                                   ¦
# -:::::::::::::::::::::::::::::::::::-


# -:::::::::::::::::::::::::::::::::::-
# ¦         V A R I A B L E S         ¦
# -:::::::::::::::::::::::::::::::::::-

# Might be obvious, but 'info()' says
# more about this.
#
end=$1
rat=$2
com=$3

# Auto '0' == no auto
auto=0

# 'band'
art=""
clr=1
del=1

# Ignore leading '[0-9]'
no_num=1

# Recursive
recur=1

opt=$(getopt --quiet -o 'rab:cdhs:' -l 'help,use,fist,no-num' -- "$@")

# -::::::::::::::::::::::::::::::::::-
# ¦         F U N C T I O N S         ¦
# -:::::::::..:::::::::::::::::::::::-

# Funktioner i grov ordning:
#
# 1. 'mid_msg'
# 2. 'l_line'
#    = Separates m. "=",
#      and shows files,
#
# 3. 'getmax'
# 4. 'err'

# Just for centering the deletion,
# dialogue. Not pretty, but it works.
#
mid_msg() {

    k="$max"
    mid="$(( (max/2) + offs ))"

    while :
    do
        # If 'k' is 'mid' to what 'l'
        # imply.
        #
        if [[ "$k" -eq "$mid" ]]; then
            if [[ "$l" -eq 0 ]]; then
                echo -e "  INPUT\n"
                break
            elif [[ "$l" -eq 1 ]]; then
                echo -e " TOTAL: $num"
                break
            fi
        else
            # "The good part"
            #
            printf " "
            (( --k ))
        fi
    done
}


# To explain what's about to deleted.
#
l_line() {

    i=0
    j=2

    printf "\n\n"

    while :
    do
        # If 'i' is 'max' delete from
        # 'j' might refer to.
        #
        if [[ "$i" -eq "$max" ]]; then

            i=0
            (( --j ))

            printf "\n"
            if [[ "$j" -eq 0 ]]; then
                break
            elif [[ "$j" -eq 1 ]]; then

                # The 'main' sequence

                l=0
                offs=6

                mid_msg

                printf "%b" "$no_name\n\n" | sed 's:\./:  :g'

                l=1
                (( offs++ ))

                mid_msg
            fi
        else
            # "The good part"
            #
            printf "="
            (( ++i ))
        fi
    done

    printf "\n"
}

# For making pretty, such pretty.
#
getmax() {
    if [[ "$in" -gt "$max" ]]; then
        max="$in"+2
    fi
}

search() {
        find "$PWD" -type f -regextype "posix-egrep" \
               -regex "$1"
}

# Print the various kinds of help that's offered.
#
info() {

    if  [[ -z "$num" ]] \
     || [[ -z "$rat" ]] \
     || [[ -z "$com" ]] \
     || [[ -z "$end" ]] \
     && [[ "$1" != "fist" ]]; then
        printf "%b"                                                         \
           "\n  USAGE:\n\n    "                                             \
           "mkopus <format> <bitrate[k]> <compression> <options> \n\n"      \
           "      [-acdr] [-b <band>] [--use] [-h|--help] [--no-num]\n\n"

        if [[ "$1" == "use" ]]; then
            exit 0
        fi

        printf "%b"                                                        \
           "\n" \
           "    NOTE: Work in progress, generally works (reassuring).\n\n" \
           "    REQUIRED:\n\n"                                             \
           "      format\\t   :   input format ex. 'ogg'\n"                \
           "      dependencies :  'ffmpeg', ('libopus')\n\n"               \
           "    REQUIRED/MODIFIABLE:\n\n"                                  \
           "      bitrate\\t   :   default: 96k      [or '-a']\n"          \
           "      compression  :   0-10 (default: 9) [or '-a']\n\n"        \
           "    OPTIONAL:\n\n"                                             \
           "      band\\t   :   any arbitrary character sequence\n"        \
           "      options\\t   :   [-abcdhr]\n\n"                          \
                                                                           \
           "    -a  use default bitrate/compression\n"                     \
           "    -b  set [band], case insensitive, regex allowed\n"         \
           "    -c  clear [band] in filename, requires '-b'\n"             \
           "    -d  deletes source files\n"                                \
           "    -h  less help\n"                                           \
           "    -r  convert files recursively and indiscriminately,\n"     \
           "        beware: also deletes, '-d', indiscriminately\n"        \
           "    --no-num    allow files without leadning digits\n"         \
           "    --use       only shows 'USAGE:'\n"                         \
           "    --help      more help\n\n"

        # The 'man' page basically.
        #
        if [[ "$1" == "extended" ]]; then
            printf "    %b"                                                 \
               "\n" \
               "[format] is needed to identify files, it is assumed\n"      \
               "         that you are in the correct directory.\n"          \
               "         'mkopus will 'cd' to '\$PWD'.\n\n"                 \
               "[bitrate]\n"                                                \
               "[comptression] are 'ffmpeg/libopus'-specific, the\n"        \
               "               defaults assumes size reduction.\n\n"        \
               "               [-a] will set these default values.\n"       \
               "                                                       \n"  \
               "               The 'opus' format allows for small size\n"   \
               "               and decent sound quality. 'mkopus' was \n"   \
               "               written to help to store a lot of audio\n"   \
               "               on disk while using less space.\n\n"         \
               "               If the source audio is good you can try\n"   \
               "               high quality values. Rule of thumb:\n"       \
               "               Garbage in = garbage out.\n\n"               \
               \
               "               'mkopus' won't produce lossless files,\n"    \
               "               like the free format 'flac'. Even if it\n"   \
               "               could it's out of scope for the above\n"     \
               "               mentioned purpose this small utility.\n\n"   \
               "               More files, less space. Not all audio\n"     \
               "               should be of the best possible quality.\n"   \
               "               Generally most won't hear any difference\n"  \
               "               unless the quality is very poor.\n\n"        \
               "               The size difference between 'opus'/'mp3',\n" \
               "               for example, is significant however.\n\n"    \
               "[band] 'band' really refers to any part of the\n"           \
               "       filename.\n\n"                                       \
               "       [A-Za-z0-9] and non-whitespace characters are \n"    \
               "       allowed. \n\n"                                       \
               "       'mkopus' is agnostic with regard to what you put\n"  \
               "       here unless it's senseless. So 'band' containing\n"  \
               "       ')-=_-+' will be processed. Escape or quote any \n"  \
               "       offending characters.\n\n"                           \
               "       [-c] clears 'band' from the file. Requires '-b'.\n"  \
               "       [-b] sets 'band' does not require '-c'.\n\n"         \
               "'--fist' works in dire situations and for audiophiles.\n\n"
        fi
    elif [[ "$1" == "fist" ]]; then
       printf "%b"          " \n" \
       "    ..··.··..         \n" \
       "  .˙ ¦  ¦  ¦ ˙_       \n" \
       "  ¦  ¦  ¦  ¦ · ´\`·   \n" \
       "  ˙·· ·· ··˙ ˙—  ·    \n" \
       "   \`          ·-˙    \n" \
       "    \`.      .´       \n" \
       "     :      :         \n" \
       "     ˙      ˙      \n\n"
    fi
}

# For the command-line options.
#
eval set -- "$opt"
unset opt

# Switch to "$DIR", or else exit.
#
cd "$PWD" || return

# "options"
#
while true
do
    case "$1" in
        '--help')

            pagers=('pager' 'less' 'more')
            for p in ${pagers[*]}
            do
                if [[ $( command -v "$p" ) ]]; then
                    info "extended" | command "$p"
                    exit 0
                fi
            done

            info 'extended'
            exit 0
            ;;
        '--fist')
            info 'fist'
            exit 0
            ;;
        '--no-num')
            no_num=0
            shift
            continue
            ;;
        '--use')
            info 'use'
            exit 0
            ;;
        '-a')
            auto=1
            shift
            continue
            ;;
        '-b')
            shift
            art="$1"
            shift
            continue
            ;;
        '-c')
            shift
            if [[ -z "$art" ]]; then
                printf "%b" "\n    Clear ['-c'] needs band ['-b']\n"\
                              "    see '-h' or '--help'\n\n"
                exit 1
            fi
            clr=0
            continue
            ;;
        '-d')
            del=0
            shift
            ;;
        '-h')
            info
            exit 0
            ;;
        '-r')
            recur=0
            shift
            continue
            ;;
        *)
            if [[ -n "$art" ]]; then
                echo "Clear: Enabled"
                clr=0
                sleep 0.4
            fi
            if [[ "$del" -eq 0 ]]; then
                echo "Deletion: Enabled"
                sleep 0.4
            fi
            if [[ "$auto" -eq 1 ]]; then
                echo "Auto: Enabled"
                sleep 0.4
            fi
            shift
            break
            ;;
    esac
done

# Assign remaining positional parameters. Not that
# pretty or efficient.
#
end="\.$1"
rat=$2
com=$3
num="$(search ".*$end$" | wc -l)"

if [[ $num == 0 ]]; then
    printf "%b" "\n    No files with the file extension '$end' found.\n\n"
    exit 1
fi

# Sets recursive, no numbers, else tracks starting with numbers
if [[ $recur -eq 0 ]]; then
    prefix="**/*"
elif [[ $no_num -eq 0 ]]; then
    prefix="*"
else
    prefix="[0-9]*"
fi

# Default directory pattern, 'work_dir' == '.'
work_dir="$prefix$end"

# Looks if '-a' and automates giving values to
# '$rat', '$com'. If not '-a' these are taken
# to be positional paramaters otherwise.
#
if [[ "$auto" == 1 ]]; then
    rat="96k"
    com=9
fi

# Iterate over any relevent parts specified above.
#
for i in $work_dir
do
    if [[ "$clr" -eq 0 ]] && [[ -n "$art" ]]; then
        N="$art"
    else
        N="\\\0"
    fi

    nname="$(printf "%s" "$PWD/$i" | sed "s/^\(\/.*\/\)000/\1/;s/$N//;s/$end/.opus/")"
    
    ffmpeg -v quiet -stats -i "$i" \
        -c:a libopus -b:a "$rat" \
        -compression_level "$com" "$nname"
done

# Delete depends on:
# Option '-d' and thus '$del'
# That the "$end"-files equals the number of 'opus'-files

if [[ "$del" -eq 0 ]]; then

    # 'no_name' = to be removed.
    no_name="$(find "$PWD" -regex "$PWD.*$end$" | sort -h)"

    # Max width used by 'l_line'
    for i in $(printf "%s" "$no_name")
    do
        in="$(printf "%s" "$i" | wc -m )"
        getmax
    done

    l_line

    printf "%b" "Delete these files? (y/n)\n> "
    read -r FILES

    if [[ -a ./yt_archive.txt ]]; then

        printf "%b" "Delete 'youtube-dl' archive '.*txt'? (y/n)\n> "
        read -r YT

        if [[ "$YT" == 'y' ]]; then
            rm -v ./yt_archive.txt
        fi
    fi

    if [[ "$FILES" == 'y' ]]; then
        # Be noisy but sort to enable troubleshooting.
        find "$PWD" -regex "$PWD.*$end$" -exec rm -v {} \; | sort -h
    fi
    exit 0

elif [[ "$del" != 0 ]]; then
    exit 0
else
    printf "%s" "Error during deletion. Info: '$end'-files = $num\n"
    exit 1
fi
